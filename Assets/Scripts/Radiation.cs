﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Radiation : MonoBehaviour
{

	public MoreMountains.CorgiEngine.Health playerHealth;
	public Image radiationFadeEffect;


	private Coroutine radiationDamageCoroutine;
	private float lastHealthValue;


	private void Start() {
		if (playerHealth == null) {
			playerHealth = GetComponent<MoreMountains.CorgiEngine.Health>();
		}

		if (radiationFadeEffect == null) {
			radiationFadeEffect = GameObject.Find("Radiation Filter").GetComponent<Image>();
		}

		lastHealthValue = playerHealth.CurrentHealth;
	}

	public void ToggleRadiationDamage(bool activated) {
		if (activated) {
			if (radiationDamageCoroutine != null) {
				return;
			} else {
				radiationDamageCoroutine = StartCoroutine(RadiationDamage());
			}
		} else {
			if (radiationDamageCoroutine != null) {
				StopCoroutine(radiationDamageCoroutine);
				radiationDamageCoroutine = null;
			}
		}
	}

	private void Update() {
		
		if (lastHealthValue != playerHealth.CurrentHealth) {
			lastHealthValue = playerHealth.CurrentHealth;

			// Alpha 0 (is ok) to 1 (not ok)
			Color c = radiationFadeEffect.color;
			radiationFadeEffect.color = new Color(c.r, c.g, c.b, 1 - (playerHealth.CurrentHealth / (float)playerHealth.InitialHealth));
		}

	}

	IEnumerator RadiationDamage() {
		while (true) {
			yield return new WaitForSeconds(1);
			Debug.Log("take damage");
			playerHealth.SilentDamage(1, null, 0, 0);
			//playerHealth.CurrentHealth--;
		}
	}

	public void HealPlayer(int value) {
		playerHealth.Heal(value);
	}
}
