﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPill : MonoBehaviour
{
	[Header("Gravitation Parameters")]
	public Vector3 direction;
	public float distance;
	[Range(0.0f, 20.0f)]
	public float speed;
	private Vector3 originalPosition;

	[Header("Pill Parameters")]
	public int restoreValue = 10;

	void Start() {
		direction.Normalize();
		originalPosition = transform.position;
	}

	void Update() {
		float oscillation = Mathf.Sin(Time.time * speed) * (distance * 2.0f);
		transform.position = (direction * oscillation) + originalPosition;
	}

	private void OnTriggerEnter2D(Collider2D collision) {
		if (collision.CompareTag("Player")) {
			Radiation radiation = collision.gameObject.GetComponent<Radiation>();

			radiation.HealPlayer(restoreValue);
			Destroy(gameObject);
		}
	}

}
