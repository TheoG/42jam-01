﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	public GameObject player;
	public ParticleSystem explosion;
	public AudioSource audioSource;

	public void QuitGame() {
		Application.Quit();
	}

	public void StartGame() {

		StartCoroutine(MovePlayer());

	}


	IEnumerator MovePlayer() {

		for (int i = 0; i < 100; i++) {
			player.transform.Translate(new Vector3(0.1f, 0f, 0f));
			yield return new WaitForSeconds(0.001f);
		}


		explosion.Play();
		if (audioSource) {
			audioSource.Play();
		}
		yield return new WaitForSeconds(3.5f);

		SceneManager.LoadScene(1);
	}

}
