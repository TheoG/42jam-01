﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerRadiationSpawnPoint : MonoBehaviour
{

	private void OnTriggerEnter2D(Collider2D collision) {
		Debug.Log("Enter" + collision.gameObject.name);

		if (collision.gameObject.CompareTag("Player")) {
			Radiation radiation = collision.gameObject.GetComponent<Radiation>();

			if (!radiation) {
				return;
			}

			radiation.ToggleRadiationDamage(true);
		}
	}
}
